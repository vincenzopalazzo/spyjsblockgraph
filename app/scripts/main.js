function main() {

  //Grading graph
  var graph = createGraphFromFile();

  var layout = Viva.Graph.Layout.forceDirected(graph, {
    springLength : 30,
    springCoeff : 0.0008,
    dragCoeff : 0.01,
    gravity : -1.2,
    theta : 1
  });

  var graphics = Viva.Graph.View.webglGraphics();


  var renderer = Viva.Graph.View.renderer(graph,
    {
      layout     : layout,
      graphics   : graphics,
      container: document.getElementById('graph-container'),
      renderLinks : true
    });

  renderer.run();

}


function createGraphFromFile() {
  var graph = Viva.Graph.graph();

  var numbarBlock = 0;

  var nameFile = readFile('resources/', '_tx.txt', numbarBlock);
  console.log('Name file: ' + nameFile);
  readWithParsing(nameFile, graph);
  /*while (nameFile != undefined){
    readWithParsing(nameFile, graph);
    numbarBlock++;
    nameFile = readFile('resources/', '_tx.txt', numbarBlock);
    console.log('Name file: ' + nameFile);
  }*/
  return graph;
}

function readWithParsing(pathFile, graph) {

  var txtFile = new XMLHttpRequest();
  txtFile.open('GET', pathFile, true);
  txtFile.onreadystatechange = function(){
    if (txtFile.readyState === 4) {  // document is ready to parse.
      if (txtFile.status === 200) {  // file is found
        //var allText = txtFile.responseText;
        var lines = txtFile.responseText.split('\n');
        for(var j = 0; j < lines.length; j++){
          var elements = lines[j].split('|-|');
          console.log('Element first: ' + elements[0]);
          graph.addNode(String(elements[0]));
          graph.addNode(String(elements[elements.length]));
          console.log('Element last: ' + elements[elements.length - 1]);
          graph.addLink(String(elements[0]), String(elements[elements.length - 1]))
        }

        console.debug('Line readed is: ' + lines);
        console.log('End file');
      }
    }
  }
  txtFile.send(null);
}

function readFile(pathInput, exstension, numbarBlock) {
  if(numbarBlock < 10){
    return pathInput + 'blk0000' + String(numbarBlock) + exstension;
  }else if(numbarBlock < 100){
    return pathInput + 'blk000' + String(numbarBlock) + exstension;
  }else if (numbarBlock < 1000){
    return pathInput + 'blk00' + String(numbarBlock) + exstension;
  }else if (numbarBlock < 10000){
    return pathInput + 'blk0' + String(numbarBlock) + exstension;
  }else if (numbarBlock < 100000) {
    return pathInput + 'blk0' + String(numbarBlock) + exstension;
  }
  return undefined;
}


