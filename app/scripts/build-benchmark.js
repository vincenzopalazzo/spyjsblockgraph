let rootPath = 'resources';


function convertMiliseconds(miliseconds, format) {
  var days, hours, minutes, seconds, total_hours, total_minutes, total_seconds;

  total_seconds = parseInt(Math.floor(miliseconds / 1000));
  total_minutes = parseInt(Math.floor(total_seconds / 60));
  total_hours = parseInt(Math.floor(total_minutes / 60));
  days = parseInt(Math.floor(total_hours / 24));

  seconds = parseInt(total_seconds % 60);
  minutes = parseInt(total_minutes % 60);
  hours = parseInt(total_hours % 24);

  switch(format) {
    case 's':
      return total_seconds;
    case 'm':
      return total_minutes;
    case 'h':
      return total_hours;
    case 'd':
      return days;
    default:
      return { d: days, h: hours, m: minutes, s: seconds };
  }
};

function buildBenchmark() {
  var benchmarkFile = new XMLHttpRequest();
  benchmarkFile.open('GET', rootPath + '/benchmark/i5-6300U@4x_3GH_gcc7.4.0.json', true);
  benchmarkFile.onreadystatechange = function(){
    if (benchmarkFile.readyState === 4) {
      if (benchmarkFile.status === 200) {
        console.debug('DATA reade: ' + benchmarkFile.responseText);
        let benchmark = JSON.parse(benchmarkFile.responseText);
        console.log(benchmark);
        var ctx = document.getElementById('benchmark-chart');
        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: ['JSON', 'GraphTx'],
            datasets: [{
              label: 'i5-6300U@4x_3GH_gcc7.4.0',
              //data: [benchmark.benchmarks[0].real_time / 1000000000.0, benchmark.benchmarks[1].real_time / 1000000000.0],
              data: [105583995668 / 1000000000.0, 88348773825 / 1000000000.0],
              backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(54, 162, 235, 0.2)',
              ],
              borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(54, 162, 235, 1)',
              ],
              borderWidth: 2
            },
              {
                label: 'i7-4790@8x4GHz-gcc-7.4.0',
                data: [72692494211 / 1000000000.0, 57735236813 / 1000000000.0],
                backgroundColor: [
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                ],
                borderColor: [
                  'rgba(255, 159, 64, 1)',
                  'rgba(255, 159, 64, 1)',
                ],
                borderWidth: 2
              }]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
      }
    }
  };
  benchmarkFile.send(null);
}
